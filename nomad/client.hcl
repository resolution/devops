client {
  enabled = true
  servers = ["1.2.3.4:4647", "5.6.7.8:4647"]
}

server {
  enabled          = true
  bootstrap_expect = 3
  server_join {
    retry_join = [ "1.1.1.1", "2.2.2.2" ]
    retry_max = 3
    retry_interval = "15s"
  }
}

consul {
  address = "unix:///var/run/consul/consul.socket"
 # auth    = "admin:password"
 # token   = "abcd1234"
}

vault {
  enabled = true
  address = "https://vault.service.consul:8200"
}
