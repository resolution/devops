*https://www.nomadproject.io/docs/agent/configuration/vault.html*
```
vault {
  enabled = true
  address = "https://vault.company.internal:8200"
}
```

ca_file (string: "") - Specifies an optional path to the CA certificate used for Vault communication. If unspecified, this will fallback to the default system CA bundle, which varies by OS and version.

ca_path (string: "") - Specifies an optional path to a folder containing CA certificates to be used for Vault communication. If unspecified, this will fallback to the default system CA bundle, which varies by OS and version.

cert_file (string: "") - Specifies the path to the certificate used for Vault communication. If this is set then you need to also set tls_key_file.

key_file (string: "") - Specifies the path to the private key used for Vault communication. If this is set then you need to also set cert_file.

### For nomad client
```vault {
  enabled     = true
  address     = "https://vault.service.consul:8200"
  ca_path     = "/etc/certs/ca"
  cert_file   = "/var/certs/vault.crt"
  key_file    = "/var/certs/vault.key"
}```