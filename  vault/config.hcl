// https://www.vaultproject.io/docs/configuration/storage/consul.html

ui = true
storage "consul" {
  address = "unix:///var/run/consul/consul.socket" 
  path    = "vault/"
  service = "vault"
}
listener "tcp" {
  address     = "0.0.0.0:8200"
  tls_disable = 1
  tls_cert_file = "/etc/certs/vault.crt"
  tls_key_file  = "/etc/certs/vault.key"
tls_client_ca_file = "/etc/certs/vault_ca.crt"
}

telemetry {
  statsite_address = "127.0.0.1:8125"
  disable_hostname = true
}



