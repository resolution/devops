# Server config
## install
- установить утилиты

```
apt-get install jq curl unzip git
```
- скачать consul nomad vault
- распаковать 
- бинарники скопировать в /usr/local/bin/
- создать пользователя consul
```
sudo useradd -r -s /bin/false consul
```
- создать workdir
```
mkdir /var/run/consul/data
chmod consul:consul /var/run/consul/data
chmod 700  /var/run/consul/data
```
- создать config dir
```
mkdir /etc/consul.d
```
- создать consul.socket файл
```
mkdir /var/run/consul
chown consul:operator /var/run/consul
```

## Consul
ENV
- CONSUL_HTTP_TOKEN=aba7cbe5-879b-999a-07cc-2efd9ac0ffe
- CONSUL_HTTP_AUTH=operations:JPIMCmhDHzTukgO6
- CONSUL_HTTP_SSL=true
Ports
- dns - The DNS server, -1 to disable. Default 8600.
- http - The HTTP API, -1 to disable. Default 8500.
- https - The HTTPS API, -1 to disable. Default -1 (disabled).
   - need certs   
```
"addresses": {
    "https": "0.0.0.0"
  },
  "ports": {
    "https": 8080
  },
  "key_file": "/etc/pki/tls/private/my.key",
  "cert_file": "/etc/pki/tls/certs/my.crt",
  "ca_file": "/etc/pki/tls/certs/ca-bundle.crt" 
```
- serf_lan - The Serf LAN port. Default 8301.
- serf_wan - The Serf WAN port. Default 8302. Set to -1 to disable. Note: this will disable WAN federation which is not recommended. Various catalog and WAN related endpoints will return errors or empty results.
- server - Server RPC address. Default 8300.


### TODO
 - telemetry circonus signup
 - chech translate_wan_addrs

## Nomad
*https://www.nomadproject.io/docs/vault-integration/index.html*

ENV
- VAULT_TOKEN=f02f01c2-c0d1-7cb7-6b88-8a14fada58c0

```nomad agent -config=nomad.hcl```
## Vault
vault operator init  // save master keys 
https://www.vaultproject.io/guides/secret-mgmt/dynamic-secrets.html