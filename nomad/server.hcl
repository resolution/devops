client {
  enabled = false
}

server {
  enabled          = true
  bootstrap_expect = 3
  server_join {
    retry_join = [ "1.1.1.1", "2.2.2.2" ]
    retry_max = 3
    retry_interval = "15s"
  }
}

consul {
  address = "unix:///var/run/consul/consul.socket"
 # auth    = "admin:password"
 # token   = "abcd1234"
}

vault {
  enabled = true
  address = "https://vault.service.consul:8200"
}