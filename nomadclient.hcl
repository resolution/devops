bind_addr = "0.0.0.0"
data_dir  = "/var/run/nomad/data"
server {
  enabled          = true
  bootstrap_expect = 1
encrypt = "cg8StVXbQJ0gPvMd9o7yrg=="
authoritative_region = "global"
}
client {
  enabled       = true
  network_speed = 100
  servers = ["nomad.service.consul:4647"]
}
acl {
  enabled = true
  token_ttl = "30s"
  policy_ttl = "60s"
}
autopilot {
    cleanup_dead_servers = true
    last_contact_threshold = "200ms"
    max_trailing_logs = 250
    server_stabilization_time = "10s"
    enable_redundancy_zones = false
    disable_upgrade_migration = false
    enable_custom_upgrades = false
}

consul {
  address = "unix:///var/run/consul/consul.socket"
  //auth    = "admin:password"
  //token   = "abcd1234"
}

vault {
  enabled = true
  address = "https://vault.service.consul:8200"
  //ca_file (string: "") - Specifies an optional path to the CA certificate used for Vault communication. If unspecified, this will fallback to the default system CA bundle, which varies by OS and version.

//ca_path (string: "") - Specifies an optional path to a folder containing CA certificates to be used for Vault communication. If unspecified, this will fallback to the default system CA bundle, which varies by OS and version.

//cert_file (string: "") - Specifies the path to the certificate used for Vault communication. If this is set then you need to also set tls_key_file.

//key_file (string: "") - Specifies the path to the private key used for Vault communication. If this is set then you need to also set cert_file.
}